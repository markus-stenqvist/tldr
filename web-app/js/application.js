if (typeof jQuery !== 'undefined') {
	(function($) {
		$('#spinner').ajaxStart(function() {
			$(this).fadeIn();
		}).ajaxStop(function() {
			$(this).fadeOut();
		});
	})(jQuery);
}


// For more than one page

$('.upvotes').click(function() {

    var that = $(this);
    var val = $(this).attr("id");
    var baseUrl = getBaseURL();

    $.post(baseUrl + 'tldr/item/vote', {id: val}, function(data) {
        if(data != "false") {
            var plusOne = parseInt(that.attr('points')) + 1;
            that.attr('points', plusOne);
            that.attr('disabled', 'disabled');
            that.text(plusOne + ' poäng');
        }
    });
});


$('.item-container').click(function() {
    var url = $(this).attr('data') ? $(this).attr('data') : '';
    if(url != '')
        window.location = getBaseURL() + 'tldr/item/view/' + url;
});



//
// For the hot action page
//

$('.tooltip-points').tooltip('hide');




/*
    Useful functions
*/

function getBaseURL () {
    if(location.hostname == "localhost")
        return location.protocol + "//" + location.hostname + (location.port && ":" + location.port) + "/";
    else
        return location.protocol + "//" + location.hostname + "/";
}