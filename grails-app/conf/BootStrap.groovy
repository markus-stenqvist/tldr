import tldr.Item
import tldr.Point
import tldr.Category

class BootStrap {

    def init = { servletContext ->

        def cat1 = new Category(name: "Media")
        def cat2 = new Category(name: "Nyheter")
        def cat3 = new Category(name: "Skvaller")

        def item = new Item(title: "Nej jag har inga känslor för känslor är gay", description: 'En samling data från sidan', categories: [cat1], link: 'http://google.se', url_encoded:  'google_se', source: "google.se", author: "root", created:  new Date(), modified: new Date()).save()
        new Point(ip: '0.0.0.0', created: new Date(), item: item).save()
        new Point(ip: '0.0.0.0', created: new Date(), item: item).save()
        new Point(ip: '0.0.0.0', created: new Date().minus(1), item: item).save()

        def item2 = new Item(title: "Bloggaren Neapeah ska vara med i Big Brother", description: 'Lite testdata är väl aldrig fel', categories: [cat2], link: 'http://aftonbladet.se', url_encoded:  'aftonbladet_se', source: "aftonbladet.se", author: "root", created:  new Date(), modified: new Date()).save()

        for(i in 1..10) {
            new Point(ip: '0.0.0.0', created: new Date(), item: item2).save()
        }
        for(i in 1..4) {
            new Point(ip: '0.0.0.0', created: new Date().minus(1), item: item2).save()
        }

        def item3 = new Item(title: "Markus har 24cm, \"inget märkvärdigt\" enligt han själv ", description: 'Lite testdata är väl aldrig fel', categories: [cat3, cat1], link: 'http://expressen.se', url_encoded:  'expressen_se', source: "expressen.se", author: "root", created:  new Date(), modified: new Date()).save()
        for(i in 1..35) {
            new Point(ip: '0.0.0.0', created: new Date().minus(3), item: item3).save()
        }
    }
    def destroy = {
    }
}
