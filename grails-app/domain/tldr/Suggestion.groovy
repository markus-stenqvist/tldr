package tldr

class Suggestion {

    String title
    String email
    String content
    Date created

    static constraints = {
        title nullable: false, blank: false, size:  1..300
        email email: true, nullable: false, blank: false
    }
}
