package tldr

class Item {

    String title
    String description
    String link
    String url_encoded
    String source
    String author
    Date created
    Date modified

    static hasMany = [points: Point, categories: Category]

    static mapping = {
        sort created: "desc"
    }

    static constraints = {
        title size: 5..180, blank: false, nullable: false
        link blank: false, nullable: false
        source blank: false, nullable: false
        url_encoded unique: true, nullable: false, blank: false
    }
}
