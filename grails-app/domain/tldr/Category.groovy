package tldr

class Category {

    String name

    static constraints = {
        name size: 5..180, blank: false, nullable: false, unique: true
    }
}
