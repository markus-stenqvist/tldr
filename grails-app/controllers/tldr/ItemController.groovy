package tldr

class ItemController {

    def index() {
        redirect(action: "heta")
    }

    def heta() {
        def items = Item.all

        items.each { item ->
            def points = Point.createCriteria().list() {
                and {
                    eq('item', item)
                    between('created', new Date() - 1, new Date())
                }
            }

            points.each {
                if(it.ip == request.getRemoteAddr()) {
                    item.metaClass.voted = true
                }
            }

            if(!item.hasProperty('voted')) {
                item.metaClass.voted = false
            }

            item.metaClass.points = points.size()
        }

        // Sorting after points descending order
        items = items.sort {
            -it?.points
        }

        [items: items]
    }

    def nya() {
        def items = Item.all.sort {
            it?.created
        }

        items.each { item ->
            def points = Point.createCriteria().list() {
                and {
                    eq('item', item)
                }
            }

            points.each {
                if(it.ip == request.getRemoteAddr()) {
                    item.metaClass.voted = true
                }
            }

            if(!item.hasProperty('voted')) {
                item.metaClass.voted = false
            }

            item.metaClass.points = points.size()
        }

        items.sort { x,y->
                y.created <=> x.created
        }

        [items: items]
    }

    def vote() {
        def item = Item.findById(params.id)
        def check = Point.createCriteria().count() {
            and {
                eq('item', item)
                eq('ip', request.getRemoteAddr())
            }
        }

        if(check == 0) {
            def point = new Point(ip: request.getRemoteAddr(), created: new Date(), item: item)

            if(point.save())
                render "true"
        }
        render "false"
    }

    def bidra() {
        def cats = Category.list()
        def message = params.id ? params.id : null

        [cats: cats, message: message]
    }


    def add() {

        def title = params.title
        def categories = params.category ? Category.findByName(params.category) : Category.findByName('Nyheter')
        def link = params.link
        def url_encoded = params.link.replaceAll( /[^0-9a-zA-Z ]/, '' ).tr( ' ', '-' )
        def source = new URI(link).getHost()
        def author = params.author ? params.author : 'Anonymous'
        Date created = new Date()
        Date modified = new Date()

        def item = new Item(title: title, description: 'Det finns ingen beskrivning än..', link: link, categories: [categories], url_encoded: url_encoded, source: source, author: author, created: created, modified: modified)
        def point = new Point(ip: request.getRemoteAddr(), created: created, item: item)
        item.points = [point]

        if(item.save() && point.save()) {
            return redirect(action: "bidra", id: 'success')
        }

        item.errors.each {
            println it
        }

        point.errors.each {
            println it
        }
        return redirect(action: "bidra", id: 'failure')
    }


    def view() {
        if (!params.id) {
            return redirect(action: "heta")
        }

        def item = Item.findByUrl_encoded(params.id)
        item.metaClass.pointsCount = item.points.size()

        [item: item]
    }

}
