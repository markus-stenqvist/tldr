package tldr

class FeedbackController {

    def index() {
        def message = params.id ? params.id : null
        [message: message]
    }

    def add() {
        def title = params.title
        def email = params.email ? params.email : 'anonymous@email.com'
        def content = params.feedback

        def suggestion = new Suggestion(title: title, email: email, content: content, created: new Date())

        if(suggestion.save()) {
            return redirect(index: "bidra", id: 'success')
        }

        suggestion.errors.each {
            println it
        }
        return redirect(action: "index", id: 'failure')
    }
}
