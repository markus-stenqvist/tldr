<%--
  Created by IntelliJ IDEA.
  User: markus
  Date: 2013-03-04
  Time: 15:17
  To change this template use File | Settings | File Templates.
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <title>Bidra med din egen nyhetslänk - Tldr.nu aggregerade nyheter</title>
    <meta http-equiv="Content-type" content="text/html; charset=utf-8">
    <meta name="layout" content="main">
</head>
<body>
    <div class="container bulk">
        <h2>Bidra med din egen nyhet</h2>
        <br />
        <g:if test="${message != null && message == 'failure'}">
            <div class="alert alert-danger">
                Ditt bidrag kunde tyvärr inte sparas, har du skrivit in någon felaktiv information?
            </div>
        </g:if>
        <g:if test="${message != null && message == 'success'}">
            <div class="alert alert-success">
                Ditt bidrag sparades korrekt.
            </div>
        </g:if>
        <g:form name="add" class="list-form" action="add">
            <g:textField name="title" placeholder="Titel för länken" /><br />
            <g:textField name="link" placeholder="Länkens URL" /><br />
            <select name="category">
                <option value="null">Välj en kategori..</option>
                <g:each in="${cats}">
                    <option value="${it?.id}">${it?.name}</option>
                </g:each>
            </select>
            <br />
            <g:textField name="author" placeholder="Ditt namn, frivilligt fält" /><br />
            <g:submitButton name="save" class="btn btn-success" value="Spara länk" />
        </g:form>
    </div>
</body>
</html>