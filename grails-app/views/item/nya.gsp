<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <title>Nya länkar - Tldr.nu en aggregerad nyhetstjänst på nätet</title>
    <meta http-equiv="Content-type" content="text/html; charset=utf-8">
    <meta name="layout" content="main">
</head>
<body>
<g:each status="i" var="it" in="${items}">
    <div class="container item-container" data="${it.url_encoded}">
        <div class="item">
            <div class="number">
                <p class="number-data">${i + 1}</p>
            </div>
            <div class="upvotes btn tooltip-points" points="${it?.points}" id="${it?.id}" data-toggle="tooltip" title="Tryck för att rösta!"  <g:if test="${it.hasProperty('voted') && it?.voted}">disabled="disabled"</g:if>>
                ${it?.points} poäng
            </div>
            <div class="item-content">
                <a href="${it?.link}" target="_blank" class="item-link fill-div">${it?.title}</a>
                <span class="item-source">(${it?.source})</span>
                <p class="item-author">Upplagd av ${it?.author} - <em>${it?.created}</em></p>
            </div>
            <div class="comment">
                <i class="icon-comment"></i> 10 kommentarer
            </div>
        </div>
    </div>
</g:each>
</body>
</html>