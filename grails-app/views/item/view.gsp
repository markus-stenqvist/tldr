<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <title>Heta länkar - Tldr.nu en aggregerad nyhetstjänst på nätet</title>
    <meta http-equiv="Content-type" content="text/html; charset=utf-8">
    <meta name="layout" content="main">
</head>
<body>
    <div class="container item-container">
        <div class="item">
            <div class="number">
                <p class="number-data">D</p>
            </div>
            <div class="upvotes btn tooltip-points" points="${item?.points}" id="${item?.id}" data-toggle="tooltip" title="Visar poäng satta de senaste 24 h"  <g:if test="${item.hasProperty('voted') && item?.voted}">disabled="disabled"</g:if>>
                ${item?.pointsCount} poäng
            </div>
            <div class="item-content">
                <a href="${it?.link}" target="_blank" class="item-link fill-div">${item?.title}</a>
                <span class="item-source">(${item?.source})</span>
                <p class="item-author">Upplagd av ${item?.author} - <em>${item?.created}</em></p>
            </div>
        </div>
        <div id="disqus-area" style="padding: 25px;">
            <div id="disqus_thread"></div>
            <script type="text/javascript">
                var disqus_shortname = 'tldrnews';

                // Dont edit below
                (function() {
                    var dsq = document.createElement('script'); dsq.type = 'text/javascript'; dsq.async = true;
                    dsq.src = 'http://' + disqus_shortname + '.disqus.com/embed.js';
                    (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(dsq);
                })();
            </script>
            <noscript>Enable javascript or be a suckah. <a href="http://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>
        </div>
    </div>
</body>
</html>