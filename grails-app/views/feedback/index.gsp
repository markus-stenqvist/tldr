<%--
  Created by IntelliJ IDEA.
  User: markus
  Date: 2013-03-04
  Time: 18:46
  To change this template use File | Settings | File Templates.
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <title>Ge oss feedback - Tldr.nu aggregerade nyheter</title>
    <meta http-equiv="Content-type" content="text/html; charset=utf-8">
    <meta name="layout" content="main">
</head>
<body>
<div class="container bulk">
    <h2>Ge oss feedback, idéer och förslag</h2>
    <br />
    <g:if test="${message != null && message == 'failure'}">
        <div class="alert alert-danger">
            Din feedback kunde tyvärr inte sparas, är din mail inte formaterad korrekt?
        </div>
    </g:if>
    <g:if test="${message != null && message == 'success'}">
        <div class="alert alert-success">
            Tack för din feedback, vi återkommer till dig om du lämnade någon mailinfo.
        </div>
    </g:if>
    <g:form name="add" class="list-form" action="add">
        <g:textField name="title" placeholder="Titel" /><br />
        <g:textField name="email" placeholder="E-mail, frivilligt fält" /><br />
        <g:textArea name="feedback" rows="10" style="width: 500px;" placeholder="Skriv din text här" /><br />
        <g:submitButton name="save" class="btn btn-success" value="Skicka feedback" />
    </g:form>
</div>
</body>
</html>