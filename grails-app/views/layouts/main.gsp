<!DOCTYPE html>
<!--[if lt IE 7 ]> <html lang="en" class="no-js ie6"> <![endif]-->
<!--[if IE 7 ]>    <html lang="en" class="no-js ie7"> <![endif]-->
<!--[if IE 8 ]>    <html lang="en" class="no-js ie8"> <![endif]-->
<!--[if IE 9 ]>    <html lang="en" class="no-js ie9"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html lang="en" class="no-js"><!--<![endif]-->
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<title><g:layoutTitle default="tldr.nu - aggregerade nyheter online, nuff said."/></title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<link rel="shortcut icon" href="${resource(dir: 'images', file: 'favicon.ico')}" type="image/x-icon">
		<link rel="stylesheet" href="${resource(dir: 'css', file: 'bootstrap.css')}" type="text/css">
		<link rel="stylesheet" href="${resource(dir: 'css', file: 'mobile.css')}" type="text/css">
        <link rel="stylesheet" href="${resource(dir: 'css', file: 'app.css')}" type="text/css">
		<g:layoutHead/>
		<r:layoutResources />
	</head>
	<body>

    <div class="navbar navbar-inverse navbar-fixed-top">
        <div class="navbar-inner">
            <div class="container">
                <button type="button" class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="brand" href="${createLink(controller: 'item')}">Tldr.nu</a>
                <div class="nav-collapse collapse">
                    <ul class="nav">
                        <li <g:if test="${params.action == "heta"}">class="active"</g:if>><a href="${createLink(controller: "item", action: 'heta')}">Heta</a></li>
                        <li <g:if test="${params.action == "nya"}">class="active"</g:if>><a href="${createLink(controller: "item", action: 'nya')}">Nya</a></li>
                        <li <g:if test="${params.action == "bidra"}">class="active"</g:if>><a href="${createLink(controller: "item", action: 'bidra')}">Bidra</a></li>
                    </ul>
                    <ul class="nav pull-right">
                        <li <g:if test="${params.controller == "feedback"}">class="active"</g:if>><a href="${createLink(controller: 'feedback')}">Lämna förslag</a></li>
                    </ul>
                </div><!--/.nav-collapse -->
            </div>
        </div>
    </div>

    <div class="container">
        <g:layoutBody/>
    </div>
		<div class="footer" role="contentinfo"></div>
		<div id="spinner" class="spinner" style="display:none;"><g:message code="spinner.alt" default="Loading&hellip;"/></div>
		<script src="${resource(dir: 'js', file: 'jquery-1.9.1.min.js')}"></script>
        <script src="${resource(dir: 'js', file: 'bootstrap.min.js')}"></script>
        <script src="${resource(dir: 'js', file: 'application.js')}"></script>
        <script type="text/javascript">
            var disqus_shortname = 'tldrnews';

            /* * * DON'T EDIT BELOW THIS LINE * * */
            (function () {
                var s = document.createElement('script'); s.async = true;
                s.type = 'text/javascript';
                s.src = 'http://' + disqus_shortname + '.disqus.com/count.js';
                (document.getElementsByTagName('HEAD')[0] || document.getElementsByTagName('BODY')[0]).appendChild(s);
            }());
        </script>
		<r:layoutResources />
	</body>
</html>
